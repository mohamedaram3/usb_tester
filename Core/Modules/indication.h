/*
 * indication.h
 *
 *  Created on: Jan 18, 2022
 *      Author: KARMA
 */

#ifndef INDICATION_H_
#define INDICATION_H_


#define INDICATION_TYPES_QTY	(4)

typedef enum {
	INDICATION_OFF,
	INDICATION_HOLD,
	INDICATION_STARTING,
	INDICATION_ACTIVE
}indication_t;


void INDICATION_Init(void);
void INDICATION_InitEvent(void);
void INDICATION_DeinitEvent(void);
void INDICATION_ActiveEvent(void);
void INDICATION_Idle(indication_t indType);

#endif
