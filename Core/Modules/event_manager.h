/*
 * event_manager.h
 *
 *  Created on: Jan 18, 2022
 *      Author: KARMA
 */

#ifndef EVENT_MANAGER_H_
#define EVENT_MANAGER_H_

#include <stdint.h>
#include <stdbool.h>

#define EVENT_CODE_NUM_LOCK_ID		(0x1)
#define EVENT_CODE_CAPS_LOCK_ID		(0x2)
#define EVENT_CODE_SCROLL_LOCK_ID	(0x4)


typedef enum {
	EVENT_CODE_NONE 	= 0,
	EVENT_CODE_INIT 	= 1 << 0,
	EVENT_CODE_DEINIT	= 1 << 1,
	EVENT_CODE_ACTIVE	= 1 << 2
}event_code_t;

void	EVENT_MANAGER_AddCode(event_code_t code);
bool	EVENT_MANAGER_CheckCode(event_code_t code);
void 	EVENT_MANAGER_ResetAllCodes(void);

void	EVENT_MANAGER_SetEventId(uint8_t id);
uint8_t	EVENT_MANAGER_GetEventId(void);
bool 	EVENT_MANAGER_CheckEventIdChange(uint32_t eventIdMask);

bool    EVENT_MANAGER_CheckActive(void);

#endif
